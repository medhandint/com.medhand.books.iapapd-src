<?xml version="1.0"?>
<xsl:stylesheet xmlns:m="http://www.w3.org/1998/Math/MathML" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="m" version="1.0">

<!--
	 *
	 * Copyright (c) 2005+ MedHand International AB
	 * All rights reserved.
	 *
	 ***************************************************************
	 *
	 * @(#)wabcpp_to_mxml.xsl
   *
   * DESCRIPTION:
   * ======================
   * Stylesheet for converting  dth XML to MedHand XML(MXML). 
   *
   * CHANGES:
   * ======================
	 -->


<!-- PRE-DEFINED FUNCTION TEMPLATES -->

<xsl:template name="getPreviousAttributes">
	<xsl:choose>
		<xsl:when test="@*">
			<xsl:for-each select="@*">
				<xsl:text> </xsl:text><xsl:value-of select="name()"/>=<xsl:text>"</xsl:text><xsl:value-of select="."/><xsl:text>"</xsl:text>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text></xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="*" mode="serialise">
  <xsl:text>&lt;</xsl:text>
  <xsl:value-of select="name()" />
  <xsl:for-each select="@*">
    <xsl:text> </xsl:text>
    <xsl:value-of select="name()" />
    <xsl:text>="</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>"</xsl:text>
  </xsl:for-each>
  <xsl:text>></xsl:text>
  <xsl:apply-templates select="node()" mode="serialise" />
  <xsl:text>&lt;/</xsl:text>
  <xsl:value-of select="name()" />
  <xsl:text>></xsl:text>
</xsl:template>

<!-- PRE-DEFINED FUNCTION TEMPLATES *END*-->

<!-- variable flag for moving misplaced process instructions or not -->
<xsl:variable name="move_pi" select="'1'"/>

<!-- book id variable -->
<xsl:variable name="book-id"><xsl:value-of select="/book/@id"/></xsl:variable>

<!-- function returning box background color -->
<xsl:template name="getBoxBackgroundColor">
	<xsl:param name="chapter-id"/>
	<xsl:attribute name="class">
		<xsl:text>outline-box-background-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning section title color -->
<xsl:template name="getSectionTitleColor">
	<xsl:attribute name="class">
		<xsl:text>section-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning part title color -->
<xsl:template name="getPartTitleColor">
	<xsl:attribute name="class">
		<xsl:text>part-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning chapter title color -->
<xsl:template name="getChapterTitleColor">
	<xsl:attribute name="class">
		<xsl:text>chapter-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div1 title color -->
<xsl:template name="getDiv1TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div1-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div2 title color -->
<xsl:template name="getDiv2TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div2-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div3 title color -->
<xsl:template name="getDiv3TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div3-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div4 title color -->
<xsl:template name="getDiv4TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div4-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning div5 title color -->
<xsl:template name="getDiv5TitleColor">
	<xsl:attribute name="class">
		<xsl:text>div5-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<!-- function returning text formatting -->
<xsl:template name="getFormatedText">
	<em>
		<xsl:attribute name="class">
				<xsl:text>text-foreground-color</xsl:text>
		</xsl:attribute>
		<xsl:choose>
			<xsl:when test="ancestor::note"> 
				<b>
					<xsl:apply-templates/>
				</b>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</em>
</xsl:template>

<!-- MANIFEST ELEMENTS TEMPLATES -->
<xsl:template match="manifest">
	<book pres-type="hard">
		<title>
			<xsl:value-of select="@title"/>
		</title>
		<img type="frontpage" src="frontpage.bmp"/> 
		<!-- PARAMETERS FOR LINEAR READING -->     
        <lr lrTop="1" lrBottom="1"/>
		<!-- PARAMETERS FOR LINEAR READING *END* --> 
		
		<xsl:apply-templates/>
	</book>
</xsl:template>

<xsl:template match="manifest-file">
<xsl:variable name="g-id" select="generate-id(.)"/>
	<container pres-type="hard" p-id="{$g-id}">
		<title search-class="title"><xsl:value-of select="@title"/></title>
		<xsl:apply-templates select="document(@src)"/>
	</container>
</xsl:template>


<!-- USER-DEFINED ELEMENTS TEMPLATES -->

<xsl:template match="html">
<xsl:apply-templates select="child::body"/>
</xsl:template>

<xsl:template match="body">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
 <xsl:apply-templates select="child::div"/>
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="div">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>
 <!-- 
 	<xsl:apply-templates select="child::h1|child::h2|child::h3|child::h4|child::p|child::table|child::ul|child::ol"/>
 -->
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="sect1">
		<container pres-type="soft" >
				<title search-class="title"> <b> <xsl:value-of select="child::h1"/> </b></title>
				<xsl:apply-templates/>
		</container>
</xsl:template>

<xsl:template match="h1">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>


<xsl:template match="h2">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<!--  <p> <xsl:apply-templates/> </p> -->
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

 
<xsl:template match="h3">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	
	<p><b><xsl:apply-templates/></b></p>
	
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="h4">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<p><i>
	<xsl:apply-templates/>
	</i></p>
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>


<xsl:template match="p">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<p>
		 	<xsl:apply-templates/>
		</p>
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>		
</xsl:template>


<xsl:template match="span">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
				 	<xsl:apply-templates/>
		
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>		
</xsl:template>
<xsl:template match="a">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xref type="www" ref="{@href}">
			<xsl:apply-templates/>
		</xref>
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>		
</xsl:template>
<xsl:template match="em">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>		
</xsl:template>
<xsl:template match="symbol">
	
	<symbol type="image" src="{@src}"/>
	
</xsl:template>
<xsl:template match="strong">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>		
</xsl:template>
<xsl:template match="img">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	
	<img type="normal" src="{@src}" xmlns:xlink="http://www.w3.org/1999/xlink"/>
	
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>		
</xsl:template>

<xsl:template match="table">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<container pres-type="none">
	
<!--  Removed because it prints all table content as title
		<title class="title-color" search-class="table">
			<xsl:value-of select="."/>
		</title>
-->
		<xsl:variable name="bdr" select="@border"/>
		<table border="{$bdr}">
		<xsl:if test="child::thead/tr">
			<xsl:for-each select="child::thead/tr">
			<tr>
			<xsl:for-each select="td">	
					<td colspan="{@colspan}"><xsl:apply-templates/></td>
			</xsl:for-each>
			</tr>
			</xsl:for-each>
		</xsl:if>
			<xsl:if test="child::tbody/tr">
			<xsl:for-each select="child::tbody/tr">
			<tr>
			<xsl:for-each select="td">	
					<td colspan="{@colspan}"><xsl:apply-templates/></td>
			</xsl:for-each>
			</tr>
			</xsl:for-each>
			</xsl:if>
		</table>
	</container>		
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="al">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<list>
	<!-- set list type -->
	<xsl:attribute name="type">
		<xsl:text>alfa</xsl:text>
	</xsl:attribute>
	<xsl:apply-templates select="child::li"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="cl">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<list>
	<!-- set list type -->
	<xsl:attribute name="type">
		<xsl:text>circle</xsl:text>
	</xsl:attribute>
	<xsl:apply-templates select="child::li"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="sl">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<list>
	<!-- set list type -->
	<xsl:attribute name="type">
		<xsl:text>square</xsl:text>
	</xsl:attribute>
	<xsl:apply-templates select="child::li"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="ul">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<list>
	<!-- set list type -->
	<xsl:attribute name="type">
		<xsl:text>bullet</xsl:text>
	</xsl:attribute>
	<xsl:apply-templates select="child::li"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="ol">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<list>
	<!-- set list type -->
	<xsl:attribute name="type">
		<xsl:text>number</xsl:text>
	</xsl:attribute>
	<xsl:apply-templates select="child::li"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match=" li">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<item>
		<!-- item content -->
			<p><xsl:apply-templates/></p>
		</item>
<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="b">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<b>
			<xsl:apply-templates/>
		</b>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<!-- external link -->
<xsl:template match="url">
	
		<!-- url variable -->
		<xsl:variable name="url">
			<xsl:choose> 
				<xsl:when test="@href" >
					<xsl:value-of select="@href"/>
				</xsl:when>			
				<xsl:when test="@webUrl">
					<xsl:value-of select="@webUrl"/>
				</xsl:when>
				<xsl:when test="@canonical">
					<xsl:value-of select="@canonical"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<!-- external xref -->
		<xref type="www" ref="{$url}">
			<xsl:apply-templates/>
		</xref>
	
</xsl:template>

<!-- italic -->	
<xsl:template match="i">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<i>
			<xsl:apply-templates/>
		</i>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<!-- subscript -->	
<xsl:template match="sub">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<sub>
			<xsl:apply-templates/>
		</sub>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<!-- superscript -->	
<xsl:template match="sup">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<sup>
			<xsl:apply-templates/>
		</sup>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="u">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<u>
			<xsl:apply-templates/>
		</u>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="br">
	
</xsl:template>



<!-- USER-DEFINED ELEMENTS TEMPLATES *END*-->

<!-- UNDEFINED ELEMENT TEMPLATES -->

<!-- This is were a unndefined element ends up. -->
<xsl:template match="*">
	[%WARNING% message=No stylesheet entry for: <xsl:value-of select="name()"/>]
</xsl:template>

<!-- UNDEFINED ELEMENT TEMPLATES *END*-->

</xsl:stylesheet>

